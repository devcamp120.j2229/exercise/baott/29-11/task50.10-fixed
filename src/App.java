public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // This is comment
        String  appName = "Devecamp will help everyone to know coding";
        /*
         This is comment for multi line
         line 1.
         line 2.
         */
        System.out.println("Hello, World! " + appName.length()); // This is a comment
        System.out.println("Hello, World! " + appName.toUpperCase()); // This is a comment
        System.out.println("LOWERCASE: " + appName.toLowerCase()); // This is a comment

    }
}
